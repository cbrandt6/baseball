import pandas as pd
import numpy as np


def calc_batter_score(batter):
    # Batter stats: LD%, HR%, wOBA
    # wOBA has .4 weight, others have .3 weight respectively
    # Use second stats library for wOBA
    pass


def calc_pitcher_score(pitcher):
    # TODO Calculate pitcher score
    # BB/9, K/9, WHIP, ERA
    # BB/9 is walks per 9 innings
    # K/9 is strikeouts per 9 innings
    # ERA is runs given up per 9 innings
    # Extracting all required stats from df
    WHIP = np.float(pitcher['WHIP'])
    BB = np.float(pitcher['BB'])
    K = np.float(pitcher['K'])
    ERA = np.float(pitcher['ERA'])

    # Divide BB and K by 9
    BB = BB/np.float(9.0)
    K = K/np.float(9.0)


def main():

    # TODO combine data for the two batter csv's
    # TODO Handle differing names in the expected stats csv
    # pd.set_option('display.max_columns', 500)
    # Import batter data into a data frame
    batters_df = pd.read_csv('stats/mlb-player-stats-Batters.csv')

    # Import pitcher data into a data frame
    pitchers_df = pd.read_csv('stats/mlb-player-stats-P.csv')

    # Compose a team of batters
    team_one, team_two = input("Enter two teams, seperated by a space: ").split()

    # The batters df is searched for player where their team column matches the teams input
    team_one_bats = batters_df.loc[batters_df['Team'] == team_one]
    team_two_bats = batters_df.loc[batters_df['Team'] == team_two]

    # Find the pitchers for the teams
    team_one_pitch = pitchers_df.loc[pitchers_df['Team'] == team_one]
    team_two_pitch = pitchers_df.loc[pitchers_df['Team'] == team_two]

    # Calculate Score for each batter on both teams
    # Each team will have lists to store batter and pitcher scores
    team_one_bscores = []
    team_two_bscores = []
    # Pass each row of the data frame to the calc batter function
    # So that the function can easily access the stats for a given batter
    for idx, row in team_one_bats.iterrows():
        team_one_bscores.append(calc_batter_score(row))

    for idx, row in team_two_bats.iterrows():
        team_two_bscores.append(calc_batter_score(row))

    # Lists for pitcher scores
    team_one_pscores = []
    team_two_pscores = []
    # Iterate through pitcher data frames and calculate scores
    for idx, row in team_one_pitch.iterrows():
        team_one_pscores.append(calc_pitcher_score(row))

    for idx, row in team_two_pitch.iterrows():
        team_two_pscores.append(calc_pitcher_score(row))

    # TODO Sum batter scores for team
    # TODO subtract pitcher scores from bat lineup score
    # TODO Determine winner


main()

